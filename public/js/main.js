/**
 * Ajax Controller
 * Responsible for all page update request
 */


//Reference to PHP file that receives the request
var _controller = 'index.php';


/**
* Sends the request to the controller 
* outputs the return in the received element and type
* @param {string}   request           request to complete
* @param {array}    arguments_array   any and all requested arguments to complete request
*/
function process_request(_request, _arguments_array)
{      
   //JSON Object - holds the key => value relation of passed arguments
   var arguments = {};
   arguments['request'] = _request;	  
   
   //Creates the JSON arguments object
   if(_arguments_array)
   {
      for(var i = 0; i < _arguments_array.length; i++)
      {	        
          var key   = _arguments_array[i][0];
          var value = _arguments_array[i][1];
          arguments[key] = value; 
      }      
    }//arguments
    
    $.post(
           _controller,
           arguments,         
           function(_results)
           {   
               try
               {
                 var json = JSON.parse(_results); 

                 $.each(json,
                     function(objects, content)
                     {  
                        var result_array = new Array();

                        $.each(content,
                           function(key, value)
                           {
                                  result_array[key] = value;
                           }                     
                        );//each object

                        var element = result_array['element'];
                        var data    = result_array['data'];
                        var type    = result_array['type'];

                        if(type === 'html')
                        {
                           $(element).html(data); 
                        } 
                        else if(type === 'append')
                        {
                           $(element).append(data); 
                        } 
                        else if(type === 'val')
                        {
                           $(element).val(data); 
                        }
                      }//callback - json each	
                  );//each json
               } // try
               catch(err)
               {
                  alert('invalid JSON enconded object = ' + _results + '. Error: ' + err);					
               }		                     
	   }//callback post
	); //post
} //process_request()	


/**
 * Processes the request sent by the action button press
 * @param   {string}  _request   request to make
 */
function on_button_click(_request)
{   
   process_request(_request);
} //on_button_click()