<?php

/**
 * Handles the flow and logic of the Psychic Poker Player Game
 * Receives the request from the main.php
 * Process the request by sending the request to the Modle 
 * Models sends the variables to complete request
 * Tells the view to render the request 
 * @author nlaracuente
 */
class PsychicPokerPlayerController 
{      
   private $_model;
   private $_view;   
   private $_request = '';  
   
   
   /**
    * If session exists - loads the serialized model & view
    */
   public function __construct() 
   {
      if( SessionsHandler::get_session('controller') != false )
      {
         $unserialized = unserialize( SessionsHandler::get_session('controller') );
         $this->_model = $unserialized['model'];
         $this->_view  = $unserialized['view'];
      }         
   } //constructor()
   
   
   /**
    * Executes all logic for the received request 
    * @param string  $_request  contains a request from tje RequestList Class to complete
    */
   public function process_request($_request)
   {
      $this->_request = $_request;
      
      switch($_request)
      {
         case RequestList::INIT_REQUEST:           
            $this->_model = new PsychicPokerPlayerModel();    
            $this->_view  = new PsychicPokerPlayerView(); 
            $this->_model->init_load($this);              
         break;
      
         case RequestList::NEW_GAME_REQUEST:
         case RequestList::DEAL_CARDS_REQUEST:
         case RequestList::ANALYZE_HAND_REQUEST:
         case RequestList::EVALUATE_HAND_REQUEST:   
            $this->_model->$_request();                     
         break;
      
         default:
            die("$_request is an invalid request. Cannot process request");
      }
      
      $this->save_session_data();
      $this->_view->render($this->_request);   
   } //process_request()
     
   
   /**
    * Calls the set_variable in the view to save the variables
    * received from the Model
    * @param string   $_name    name  of variable
    * @param varies   $_value   value of variable
    */
   public function set_variable($_name, $_value)
   {       
       $this->_view->set_variable($_name, $_value);
   } //set_variable()   
   
   
   /**
    * Serializes all session objects: model & view
    * Saves the serialized variables to the current session
    */
   private function save_session_data() 
   {      
      $session_array = array( 'model' => $this->_model,
                              'view'  => $this->_view  
                             );    
      
      $serialized = serialize($session_array);
      SessionsHandler::set_session('controller', $serialized);        
   } //save_session_data()
   
} //class