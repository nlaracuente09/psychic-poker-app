<?php

/**
 * Holds the values of all suits and face value
 * Contains the full deck of cards 
 * Handles deck creations, hand dealings, and discarding of cards
 * Processes the suggestions for best hands
 * @author nlaracuente
 */
class PsychicPokerPlayerModel 
{
   private $_suits_array     = array("C", "D", "H", "S");
   private $_faceValue_array = array("A", 2, 3, 4, 5, 6, 7, 8, 9, 10, "J", "Q", "K");
   private $_fullDeck_array  = array();
   
   //Types of actions (requests)
   const NEW_ACTION        = 'new';
   const DEAL_ACTION       = 'deal';
   const ANALYZE_ACTION    = 'analyze';
   const EVALUATE_ACTION   = 'evaluate';
   const PLAY_AGAIN_ACTION = 'replay';
   
   //Messages displayed 
   private $_message_array = array(
            self::NEW_ACTION      => 'Welcome to Psychic Poker. 
                                      Where I am the psychic and you 
                                      are always the winner. Shall we be begin?',
            self::DEAL_ACTION     => 'Press the button to have the 
                                      dealer deal you a hand',
            self::ANALYZE_ACTION  => 'When you are ready, press the 
                                      button again and, using my unbelievable
                                      super-human psychic abilities, I
                                      will select the cards that need to 
                                      be discard for the best possible hand',
            self::EVALUATE_ACTION => 'Ladies and Gentelmen, once again,
                                      I, your outstanding AI, have done it!
                                      The cards highlighted are the ones 
                                      that will be discarded to get the 
                                      best possible hand. Go ahead, press
                                      the button and be amazed!',
          self::PLAY_AGAIN_ACTION => 'Go on, press it. You know you want to see
                                      that again.'   
                                   );
   
   //Action Button Text
   private $_buttonText_array = array( self::NEW_ACTION        => 'New Game',
                                       self::DEAL_ACTION       => 'Deal',
                                       self::ANALYZE_ACTION    => 'Analyze',
                                       self::EVALUATE_ACTION   => 'Evaluate',
                                       self::PLAY_AGAIN_ACTION => 'Play Again'
                                      );
   
   //Action Button Requests
   private $_buttonAction_array = array(
                    self::NEW_ACTION        => RequestList::NEW_GAME_REQUEST,
                    self::DEAL_ACTION       => RequestList::DEAL_CARDS_REQUEST,
                    self::ANALYZE_ACTION    => RequestList::ANALYZE_HAND_REQUEST,
                    self::EVALUATE_ACTION   => RequestList::EVALUATE_HAND_REQUEST,
                    self::PLAY_AGAIN_ACTION => RequestList::NEW_GAME_REQUEST
                                        );
  
   private $_user;
   private $_dealer;
   private $_psychic;
   
   //Maximum Cards the user can hold
   private $_totalUserCards = 5; 
   
   //Dealer's total card is twice the user's hand
   private $_totalDealerCards;    
   
   //A reference to the controller. Used to send variables required by the view
   private $_controller; 


   /** Getters - returns the value of the requested property **/
  
   public function get_dealer_deck() { return $this->_dealer->get_deck(); }
   
   /** end Getters **/
     
   
   /**
    * Initial Application Load
    * Creates the full deck of cards
    */
   public function init_load($_controller)
   {
      $this->_controller = $_controller;
            
      $this->_user    = new User();
      $this->_dealer  = new Dealer();     
      $this->_psychic = new PsychicPlayer(); 
     
      $this->_totalDealerCards = $this->_totalUserCards * 2;
      $this->_fullDeck_array   = $this->create_full_deck($this->_suits_array, 
                                                         $this->_faceValue_array);      
      $this->update_gui(self::NEW_ACTION);
   } //init_load()  
   
   
   /**
    * Resets  the psychic player's booleans for possible hands 
    * Cleares the user's deck - resets it to "empty"
    * Creates and assigns a new random deck for the dealer
    * Updates GUI to 'deal'
    */
   public function new_game()
   {     
      $this->_psychic->reset();
      $this->_controller->set_variable( 'userDeck_array', array() );  
      $this->_controller->set_variable( 'bestHandMessage', "" );
       
      $this->_dealer->set_deck( $this->get_random_deck( $this->_fullDeck_array, 
                                                        $this->_totalDealerCards) ); 
      
      $this->_controller->set_variable('dealerDeck_array', $this->_dealer->get_deck());      
      
      $this->update_gui(self::DEAL_ACTION);
   } //new_game()
   
   
   /**
    * Based on user's max hand, collects the cards the top of the dealer's deck
    * shifting the value from the dealers into the user's deck
    * Updates the GUI to 'analyze'
    */
    public function deal_cards()
    {
      $cards_array = $this->_dealer->deal_cards($this->_totalUserCards);
      $this->_user->set_deck($cards_array);
      
      $this->_controller->set_variable( 'userDeck_array',   $cards_array );
      $this->_controller->set_variable( 'dealerDeck_array', 
                                        $this->_dealer->get_deck() );
      
      $this->update_gui(self::ANALYZE_ACTION);
    } //deal_cards()

    
    /**
     * Tells the Psychic Player to analyze the decks to get the best hand
     * Updates the user's deck card to show cards "selected" to be discarded (if any)
     * Updates GUI to 'Evaluate Hand' (final step)
     */
    public function analyze_hand()
    {
       $this->_psychic->analyze_decks( $this->_user->get_deck(),
                                       $this->_dealer->get_deck() );  
       
       $keepCards_array = $this->_psychic->get_card_keys_to_hold_array();
       
       //Discards all cards when none were kept
       if(count($keepCards_array) == 0 )
       {
          //Empty array will cause all cards to be removed
          $this->update_cards_to_discard( array() );
       }
       else 
       {
          $this->update_cards_to_discard($keepCards_array);
       }
                   
       $this->_controller->set_variable( 'userDeck_array', 
                                         $this->_user->get_deck() );
      
       $this->update_gui(self::EVALUATE_ACTION);       
    } //analyze_hand()
    
    
     /**
      * Gets the card keys collected the by the Psychic Player that
      * are kept on the users deck during the discard process
      * Loops through the user's deck replacing the non-selected cards with "null"
      * Tells the dealers to deal a hand based on the total "discarded" cards
      * Updates the dealer's deck to reflected the cards removed from its deck
      * Updates the user's deck to replace the null values with the dealt card
      * Updates the GUI to "play again" to allow for a new round 
      */
     public function evaluate_hand()
     {
        $_keptCards_array = $this->_psychic->get_card_keys_to_hold_array();
        $this->_user->discard_cards($_keptCards_array); 
        
        $newCardsTotal = $this->_user->get_total_discarded_cards();
        $cards_array   = $this->_dealer->deal_cards($newCardsTotal);
        
        $this->_user->update_deck($cards_array);       
        
        $this->_controller->set_variable( 'userDeck_array', 
                                          $this->_user->get_deck() );
        
        $this->_controller->set_variable( 'dealerDeck_array', 
                                          $this->_dealer->get_deck() );
        
        $this->_controller->set_variable( 'bestHandMessage', 
                                          $this->_psychic->get_best_hand() );
        
        $this->update_gui(self::PLAY_AGAIN_ACTION);
     } //evaluate_hand()


    /**
     * Updates the disaplyed message, button text, and action to the specified action
     * @param string   $_action   action (request) to update to
     */
    private function update_gui($_action)
    {
       $this->_controller->set_variable( 'message', 
                               $this->_message_array[$_action] );
       $this->_controller->set_variable( 'actionButtonText', 
                               $this->_buttonText_array[$_action] );  
       $this->_controller->set_variable( 'actionButtonRequest', 
                               $this->_buttonAction_array[$_action] );
    } //update_gui()


   /**
    * Loops through all suits assigned all the values in the face value array
    * to each individual suits to create a playing card
    * Returns an array containing all cards objects that form the entire deck
    * @param  array $_suits_array       card suits
    * @param  array $_faceValue_array   card face value
    * @return array 
    */
   private function create_full_deck(array $_suits_array, array $_faceValue_array)
   {
      $fullDeck_array = array();

      foreach($_suits_array as $suit)
      {
         foreach($_faceValue_array as $value)
         {
            //Rank is used to determine sequence value
            $rank = $value;
            
            if(is_string($rank) )
            {
               switch ($rank) 
               {
                  case 'A':
                     $rank = 1;
                  break;
                  case 'T':
                     $rank = 10;
                  break;
                  case 'J':
                     $rank = 11;
                  break;
                  case 'Q':
                     $rank = 12;
                  break;
                  case 'K':
                     $rank = 13;
                  break;
               }
            } //if rank is NaN
            
            $card = new Card($suit, $value, $rank);
            $fullDeck_array[] = $card;
         } //foreach face value
      } //foreach suit

      return $fullDeck_array;
   } //create_full_deck()


   /**
    * Shuffles the copy of the original deck of cards to create a random deck
    * Based on the total provided, selects random cards to add to the new deck
    * During each iternation removes the selected card from the copy of the
    * full deck to prevent duplication
    * Shuffles the final new deck to increase random deck of cards
    * Returns a new deck of cards with random card objects from full deck
    * @param  array  $fullDeck_array   Array containing all avaiable card objects
    * @param  int    $total            Total number of cards to create the deck with
    * @return array 
    */
   public function get_random_deck(array $_fullDeck_array, $_total = 0)
   {
      //Prevents non-positive-interger to be used for the loop
      $_total = abs( floor( intval($_total) ) );

      //Should int conversion fail cancel request 
      if( is_nan($_total) ) return false;

      $newDeck_array = array();
      shuffle($_fullDeck_array);

      for($i = 0; $i < $_total; $i++)
      {
         $index = rand(0, sizeof($_fullDeck_array) - 1);         
         $newDeck_array[] = $_fullDeck_array[$index];
         array_splice($_fullDeck_array, $index, 1);
      }   

      shuffle($newDeck_array);   
      return $newDeck_array; 
   } //get_random_deck()
   
   
   /**
    * Receives card keys of cards to keep in the user's hands
    * All other cards will be updated to "selected" to prepare for discard
    * @param  array   $keepCards_array    array with keys of cards to keep
    */
   private function update_cards_to_discard(array $keepCards_array)
   {      
      $userDeck = $this->_user->get_deck();
      
      for( $i = 0; $i < count($userDeck) - 1; $i++)
      {
         if( !in_array($i, $keepCards_array) )
         {
            $card = $userDeck[$i];
            if($card instanceof Card)
               $card->set_to_selected();
         }
      }      
   } //update_cards_to_discard()  
      
} //class