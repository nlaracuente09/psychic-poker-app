<div id="game-container">   
   <div id="poker-table">
      <div id="dealer" class="name-sign"> Dealer </div>
      <div id="dealer-deck">                  
      </div>
      
      <div id="table-center">
         <div id="game-message"> 
             <?php if( isset($message) ): 
                      echo $message;
                   endif;
             ?>
         </div>         
         <div class="vertical-padding"> </div>
         
         <div id="action-button-container">
            <?php if( isset($actionButtonText) && isset($actionButtonRequest) ): 
                     echo " <span id=\"action-button\"
                                  onclick=\"on_button_click('{$actionButtonRequest}')\" > 
                            $actionButtonText 
                            </span>";
                  endif;
            ?>         
         </div>
      </div>
      
      <div id="player-deck">          
      </div>     
      <div id="best-hand-message" class="name-sign"> </div>
   </div> <!--End Poker Table-->  
</div>