<?php

/**
 * Reponsible for creating/update the HTML that is output 
 */
class PsychicPokerPlayerView
{     
   //Associative array that holds all variables received from the Model
   private $_varibles_array = array();
   
   //Is convereted into a json object and passed to the calling JS 
   private $_json_array = array();
   
   
   /**
    * Creates/Updates a variable received from the Model
    * @param string   $_name    name  of variable
    * @param varies   $_value   value of variable 
    */
   public function set_variable($_name, $_value)
   {
      $this->_varibles_array[$_name] = $_value;
   } //set_variable() 
  
   
  /**
   * Extracts all variables from variables arrays
   * to convert them into individual variables
   * When request is initial load, it loads header, body, and footer
   * All other requests are considered updates and will check for updates to:
   * dealer & user's deck, message & action button updates, and best hand text
   */
   public function render($_request)
   {      
     extract($this->_varibles_array);
     
     if($_request == RequestList::INIT_REQUEST)
     {         
        include_once DEFAULT_HEADER;
        include_once (VIEW_PATH . 'pages' . DS . 'pokerTable.php');
        include_once DEFAULT_FOOTER;       
     }   
     else
     {       
       //Update Dealer's deck
       if( isset($dealerDeck_array) )
       {
          $cards = $this->update_card_deck($dealerDeck_array);            
          $this->updateJsonArray('html', "#dealer-deck", $cards);
       }       
       
       //Update displayed message
       if( isset($message) )
       {
          $this->updateJsonArray('html', "#game-message", $message);
       }
       
       //Update action button's text and action
       if( isset($actionButtonText) && isset($actionButtonRequest) )
       {
          $button = $this->update_action_button($actionButtonText, $actionButtonRequest);
          $this->updateJsonArray('html', "#action-button-container", $button);
       }
       
       //Update User's deck
       if( isset($userDeck_array) )
       {
          $cards = $this->update_card_deck($userDeck_array);         
          $this->updateJsonArray('html', "#player-deck", $cards);
       }  
       
       //Update best hand text
       if( isset($bestHandMessage) )
       {        
          $bestHand = "Best Hand: $bestHandMessage";
          $this->updateJsonArray('html', "#best-hand-message", $bestHand);
       }       
       
       //Enconde and return response
       echo $this->create_json($this->_json_array);
     }
   } //render()      
   
      
   /**
    * Creates the new action button HTML with the specified text and onclick action
    * @param type $_text      button text
    * @param type $_request   onclick request
    */
   private function update_action_button($_text, $_request)
   {
      $html = " <span id=\"action-button\"
                      onclick=\"on_button_click('{$_request}')\" > 
                $_text 
                </span>
              ";
                      
      return $html;        
   } //update_action_button()
      
   
   /**
    * Creates a new deck of cards face down
    * Returns the HTML containing all the cards
    * @param  int     $_total   Total number of cards to create
    * @return string  
    */ 
   private function update_dealer_deck($_total)
   {
       $dealers_deck = '';
       
       if( isset($_total) ) 
       {
          for($i = 0; $i < $_total; $i++)
          {
             $dealers_deck .= '<div class="face-down-card"> </div>';
          }
       }
       
       $dealers_deck .= '<div class="vertical-padding"> </div>';
       
       return $dealers_deck;      
   } //update_dealer_deck()
      
   
   /**
    * Creates the HTML for face up cards 
    * Adds class for the corresponding suit and card color based on suit
    * Returns the HTML containing all the cards
    * @param  array  $_cards_array  array of all cards
    * @return string
    */
   private function update_card_deck(array $_cards_array)
   {      
      $html = "";
      
      foreach($_cards_array as $card)
      {         
         $suit       = $card->get_suit();
         $value      = $card->get_value();
         $isSelected = $card->get_isSelected();
         
         $suitClass  = strtolower($suit) . "-card ";
         $colorClass = ($suit == "C" || $suit == "S") ? "black-card " : "red-card ";
         $selectedClass = ($isSelected) ? "highlighted-card " : "";   
         
         $class = "face-up-card " . $suitClass . $colorClass . $selectedClass;
         
         $html .= " <div class=\"player-card\">
                     <div class=\"$class\"> $value </div>
                    </div>
                   " ;
      }
      
      unset($card);      
      $html .= '<div class="vertical-padding"> </div>';      
      return $html;
   } //update_user_deck() 


   /**
    * Addas a new element to the array that will be converted into a json object
    * @param string   $_type      update type: "html, append, val"
    * @param string   $_element   name of element being updated
    * @param string   $_data      new or updated HTML data
    */
   private function updateJsonArray($_type, $_element, $_data)
   {
      $this->_json_array[] = array ( 'type'    => $_type,
                                     'element' => $_element,
                                     'data'    => $_data,
                                    );
   } //updateJsonArray()
   
   
   /**
    * Creates the json encoded object to send as response 
    * Returns the json_econded array
    * @param  array  $_array  contains a list of all HTML elements to be updated
    * @return json   
    */
   private function create_json(array $_object_array)
   {
      $json_array = array();
           
      foreach($_object_array as $object)
      {
         $json_array[] =  array( 'type'    => "{$object['type']}",
                                 'element' => "{$object['element']}", 
                                 'data'    => "{$object['data']}"
                               );
      }
      
      return json_encode($json_array);
   } //create_json()
   
} //class