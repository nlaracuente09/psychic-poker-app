Properties and Methods shared among the user and dealer objects
Unique properties and methods for each object

Properties:
$deck_array - array containing the cards they hold
$maxCards   - int of total cards they can hold

User Only Property:
$totalDiscardedCards - holds the total number of discarded cards 

Methods:
new_deck()       - creates/updates the deck to hold the new set of cards
toggle_display() - calls the toggle_display() for all cards on deck 

Dealer Method:
deal_cards() - loop - during each iteration it takes the first card off the delear's deck and saves it
               into a return deck array, reducing the dealers deck's length each time
               

User Only Method:
discard_cards() - replaces the card in the deck with a null value
                  increases the $totalDiscardedCards by one
				  
update_deck()   - receives the new set of cards to add to the deck
                  Finds the first element with a null value and replaces with the new card
				  iterates for all cards in the deck received
