Responsible for comparing the user and dealer's deck for best possible hand
Contains the names of all possible hands 
Checks for all requirements for each hand to be true

properties:
booleans for each possible hand
name of the best hand found

Methods:
compare_decks() reviews both decks for best possible hands collecting a list of possible hands
get_best_hand() based on the possible hands - compare and return the best based on hand  