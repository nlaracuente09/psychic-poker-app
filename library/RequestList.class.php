<?php

/**
 * Contains the list of all valid requests the application can proccess
 * @author nlaracuente
 */
class RequestList 
{
   const INIT_REQUEST          = 'init_load';
   const NEW_GAME_REQUEST      = 'new_game';
   const DEAL_CARDS_REQUEST    = 'deal_cards';
   const ANALYZE_HAND_REQUEST  = 'analyze_hand'; 
   const EVALUATE_HAND_REQUEST = 'evaluate_hand'; 
   
} //class