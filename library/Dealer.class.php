<?php

/** 
 * Extends Player class 
 * Holds the dealer's deck 
 * Handles the discardin of dealer's deck when dealing a card
 * @author nlaracuente
 */
class Dealer extends Player
{
    
   /**
    * Based on the total cards to deal loop through the deck removing 
    * the first card on the list 
    * Returns the new set of cards
    * @param  int    $_total   Total Cards to deal
    * @return array            
    */
   public function deal_cards($_total)
   {      
      $cards_array = array();
      
      //Prevents non-positive-interger to be used for the loop
      $_total = abs( floor( intval($_total) ) );

      //Should int conversion fail cancel request 
      if( is_nan($_total) ) return false;
      
      for($i = 0; $i < $_total; $i++)
      {
         if( count($this->_deck_array) <= 0) break;         
         $cards_array[] = array_shift($this->_deck_array);         
      }
      
      return $cards_array;
   } //deal_cards()     
   
} //class