<?php

/**
 * Main is responsible for setting up the environment (development/production)
 * Retrieves the desired request
 * Tells the controller to process the request
 */


/**
 * Check if environment is set to development mode to display errors
 * Tracks errors in the error log
 */
function set_reporting()
{
  if(DEVELOPMENT_ENVIRONMENT)
  {
      error_reporting(E_ALL);
      ini_set('display_errors', 'On');
  }
  else 
  {
      error_reporting(E_ALL);
      ini_set('display_errors', 'Off');
      ini_set('log_errors', 'On');
      ini_set('error_log', ROOT . DS . 'tmp' . DS . 'logs' . DS . 'error.log');
  }
} //set_reporting()


/**
 * Autoload all required classes
 */
function class_loader($_className)
{  
  $className = ucwords($_className) . '.class.php';
  
  $file_array = array();
  $file_array[0] = LIBRARY_PATH    . $className;  
  $file_array[1] = CONTROLLER_PATH . $className;
  $file_array[2] = VIEW_PATH       . $className;
  $file_array[3] = MODEL_PATH      . $className;

  foreach ($file_array as $file) 
  {    
     if( file_exists($file) ) { require_once $file; }
  } 
  
} //class_loader()


/** 
 * Gets the desired request
 * Creates a new controller
 * Tells controller to process the request
 */
function main()
{    
   $request = ( isset($_POST['request']) ) ?
                htmlentities($_POST['request']) : 
                RequestList::INIT_REQUEST; 
   
   $controller = new PsychicPokerPlayerController();    
   $controller->process_request($request);
} //main()

spl_autoload_register("class_loader");
set_reporting();
main();