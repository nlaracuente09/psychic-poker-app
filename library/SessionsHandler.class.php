<?php

/**
 * Handles sessions for the application
 * Saves and returns requested session variable
 * @author nlaracuente
 */
class SessionsHandler
{
   
   /**
    * Starts a session if one is not already active
    */
   public static function start_session()
   {
      if (session_id() == "")  session_start(); 
   } //start_session()


   /**
     * Returns the session value if it exists 
     * @param  string   $name     name of the session
     * @return session value or null 
     */
    public static function get_session($name)
    {
       self::start_session();
       
       if( isset($_SESSION[$name]) )
       {
           return $_SESSION[$name];
       }        
       return false;
    } //get_session()


    /**
     * Creates or updates the values of the session
     * @param string   $name     Session name
     * @param varies   $value    Session value
     */
    public static function set_session($name, $value)
    {   
       self::start_session();
       $_SESSION[$name] = $value;   
       self::close_session();
    } //set_session()    
    

    /**
     * Destroy specified session
     * @param   string   $name   Session to unset
     */
    public static function remove_session($name)
    {
        self::start_session();
        unset($_SESSION[$name]);
    } //remove_session()    


    /**
     * Unsets all session variables
     */
    public static function destroy_all_sessions()
    {
        self::start_session();
        unset($_SESSION);
        session_destroy();
    } //destroy_all_sessions()   
    

    /**
     * Closes the session
     */
    public static function close_session() 
    {
        session_write_close();
    } //close_session()   

}// class