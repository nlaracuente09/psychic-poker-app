<?php

/** 
 * Extends Player class 
 * Holds the user's deck 
 * Handles the discarding and replacing of user's hand
 * @author nlaracuente
 */
class User extends Player
{
   
   //Total cards discard - used when requesting new cards from dealer
   private $_totalDiscardedCards = 0;   
   
   
   /** Getters - Returns the value of the requested property **/
   
   public function get_total_discarded_cards(){ return $this->_totalDiscardedCards; }
   
   /**end Getters**/
   
     
   /**
    * Receives the keys for the cards to be kept
    * Loops through all cards in deck
    * For every key that is not on the kept cards array replaces the value with null
    * Increased the total discarded cards by 1 for each null value added
    * @param array $_keptCards_array  array keys of cards to be kept
    */
   public function discard_cards(array $_keptCards_array)
   {
      //Reset Discarded Total
      $this->_totalDiscardedCards = 0;
      
      foreach($this->_deck_array as $key => $cardKey)
      {
         if( !in_array($key, $_keptCards_array) )
         {
            //The application keeps an active reference to the card using sessions
            //This prevents the card from being displayed as "selected" if re-chosen in a new game
            $card = $this->_deck_array[$key];
            $card->set_to_selected(false);   
            
            $this->_deck_array[$key] = null; 
            $this->_totalDiscardedCards++;
         }
      }      
   } //discard_cards()
     
   
   /**
    * Receives the new cards to add to the deck
    * Collects the list of empty (null) elements in the current deck
    * Loops through each card in the current deck 
    * For every "null" value it finds, it shifts the value from the new deck to the current
    * Makes the new card visible
    * @param array $_newDeck_array  new cards to add to deck
    */   
   public function update_deck(array $_newDeck_array)
   {      
      $nullKeys_array = array_filter( $this->_deck_array, array($this, 'check_null_value') );
      
      foreach($nullKeys_array as $key => $value)
      {
          if( count($_newDeck_array) <=0 ) break;
          
          $card = array_shift($_newDeck_array);
          $this->toggle_display($card);
          $this->_deck_array[$key] = $card;
      }
      
      unset($key);
   } //update_deck()
   
   
   /**
    * Checks if the value passed is of null value
    * Returns true if the value is null
    * @param  Card | null   $_value   the value to be examined
    * @return boolean 
    */
   private function check_null_value($_value)
   {
      if($_value == null) return true;
      return false;
   } //check_null_value()
   
} //class