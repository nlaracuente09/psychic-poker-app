<?php

/**
 * Defines required variables for successful application startup/run
 */
define('DEVELOPMENT_ENVIRONMENT', true);
define('PUBLIC_PATH', 'public' . DS);

define('MAIN_CSS', PUBLIC_PATH . 'css' . DS . 'main.css');
define('MAIN_JS',  PUBLIC_PATH . 'js'  . DS . 'main.js');
define('JQUERY',   PUBLIC_PATH . 'js'  . DS . 'jquery.min.js');
define('IMG_PATH', PUBLIC_PATH . 'img' . DS);

define('DEFAULT_HEADER',  LIBRARY_PATH . 'header.php');
define('DEFAULT_FOOTER',  LIBRARY_PATH . 'footer.php');



define('CONTROLLER_PATH', ROOT . DS . 'application' . DS . 'controllers' . DS);
define('MODEL_PATH',      ROOT . DS . 'application' . DS . 'models'      . DS);
define('VIEW_PATH',       ROOT . DS . 'application' . DS . 'views'       . DS);
        
require_once (LIBRARY_PATH . 'main.php');