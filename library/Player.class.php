<?php

/**
 * Base class for poker players 
 * Holds the decks on hand
 * Reponsible for getting/setting the Player properties
 * @author nlaracuente
 */
abstract class Player 
{
   protected $_deck_array;  //All cards on deck (hand)
   protected $_maxCards;    //Total cards the deck can hold at one time
   protected $_name;        //Name of the player to display 
   
   
   /** Setters the value for the specified property **/ 
   
   public function set_name($_name)          { $this->_name     = (string) $_name; }     
   public function set_max_cards($_maxCards) { $this->_maxCards = (int) $_maxCards; }  
   
   public function set_deck(array $_deck_array){ $this->_deck_array = $_deck_array; }  
   
   /** end setters **/
   
   
   /** Getters - returns the value of the specified property **/   
   
   public function get_name()      { return $this->_name;}     
   public function get_max_cards() { return $this->_maxCards; }     
   public function get_deck()      { return $this->_deck_array; }  
   
   public function get_total_cards_in_deck(){ return count($this->_deck_array); }
   
   /** end setters **/
  
   
   /**
    * Loops through all cards on deck and toggles its display face (up/down)
    */
   public function toggle_display()
   {
      foreach ($this->_deck_array as $card)
      {
         if($card instanceof Card) $card->toggle_display();
      }
      
      unset($card);
   } //toggle_display()  
   
} //class
