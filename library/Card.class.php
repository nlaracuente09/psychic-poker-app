<?php

/**
 * Properties and Methods for the Cards used in the Psychic Poker Player Game * 
 * @author nlaracuente
 */
class Card 
{
   private $_suit;
   private $_value;
   private $_rank;   
   private $_isHidden   = false;
   private $_isSelected = false;
   
   
   /**
    * Sets the Cards suit, value, and rank
    * Sets initial display value - 
    * Hidden means the card shows "face down" 
    * @param type $_suit      Card's suit
    * @param type $_value     Card's value
    * @param type $_rank      Card's rank
    * @param type $_isHidden  defaults to true for face up
    */
   public function __construct($_suit, $_value, $_rank, $_isHidden = false) 
   {
      $this->_suit     = $_suit;
      $this->_value    = $_value;
      $this->_rank     = $_rank;
      $this->_isHidden = $_isHidden;
   } //constructor()
   
   
   /** Getters - Returns the value of the requested property **/
   
   public function get_suit()  { return $this->_suit;  }
   public function get_value() { return $this->_value; }
   public function get_rank()  { return $this->_rank;  }
   
   public function get_isSelected() { return $this->_isSelected;  }
   
   /** end Getters **/
   
   
   /**
    * Changes the card's isSelected value
    * Defaults to "true" to prep card for discard    
    * @param type $_isSelected  (optional) true to update card to selected status
    */
   public function set_to_selected($_isSelected = true)
   {
      $this->_isSelected = $_isSelected;
   } //set_to_discard()
   
   
   /**
    * Toggles between "true/false" for isHidden
    * True - card is facing up
    */
   public function toggle_display()
   {
      $this->_isHidden = ($this->_isHidden) ? false : true;
   } //toggle_display()
   
} //class