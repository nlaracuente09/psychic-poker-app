<?php

/**
 * Responsible for analyzing the users and dealers deck 
 * and return best possible hand
 * @author nlaracuente
 */

class PsychicPlayer 
{
   
   //Holds the index (key) if the selected cards from user's deck
   private $_cardKeysToHold_array = array();
   
   //The following are a list of booleans and arrays to hold
   //Possible lower-valued-hands and their keys found during
   //Checks of higher-level-hands 
   private $_isFourKinds  = false;
   private $_isFlush      = false;
   private $_isThreeKinds = false;   
   private $_isTwoPairs   = false;
   private $_isOnePair    = false;         
   
   //Holds potential keys based on hand type
   private $_onePair_array      = array();
   private $_threeKinds_array   = array();
   private $_fourKinds_array    = array();
   private $_flushKeys_array    = array(); 
   private $_twoPairsKeys_array = array();   
   
   //Holds the name of the best hand for current session   
   private $_bestHand = '';
      
   //Names of best hands in the descending order
   const STRAIGHT_FLUSH  = 'straight-flush';
   const FOUR_OF_A_KIND  = 'four-of-a-kind';
   const FULL_HOUSE      = 'full-house';
   const FLUSH           = 'flush';
   const STRAIGHT        = 'straight';
   const THREE_OF_A_KIND = 'three-of-a-kind';
   const TWO_PAIRS       = 'two-pairs';
   const ONE_PAIR        = 'one-pair';
   const HIGHEST_CARD    = 'highest-card';
   
   
   const PAIR_CARDS  = 2;
   const THREE_CARDS = 3;
   const FOUR_CARDS  = 4;
   const LOWEST_HIGH_CARD = 7;
   
   /** Getters - returns the values for the specified property **/
   
   public function get_card_keys_to_hold_array(){ return $this->_cardKeysToHold_array; }
   public function get_best_hand(){ return $this->_bestHand; }
   
   /**end getters**/
   
   
   /**
    * Resets the booleans back to false
    */
   public function reset()
   {
      $this->_isFourKinds  = false;
      $this->_isFlush      = false;
      $this->_isThreeKinds = false;
      $this->_isOnePair    = false; 
      $this->_isTwoPairs   = false;
   }
   
   
   /**
    * Compares the user's hand versus the dealer's deck
    * Calucate the best possible hands
    * Selects  the cards to discard and returns their index 
    * Cards are not removed from the deck until final "hit" - only updated to show
    * they will be removed 
    * @param  array $_userDeck_array    array of user's deck containing card objects
    * @param  array $_dealerDeck_array  array of dealer's deck containing card objects    
    */
   public function analyze_decks(array $_userDeck_array, array $_dealerDeck_array)
   {      
      $suits_array  = $this->get_property_array('get_suit',  $_userDeck_array);
      $values_array = $this->get_property_array('get_value', $_userDeck_array);
      $ranks_array  = $this->get_property_array('get_rank',  $_userDeck_array);       
      
      $this->find_flush_hand($suits_array, $_userDeck_array, $_dealerDeck_array);
      $this->find_same_value_cards($values_array, $_userDeck_array, $_dealerDeck_array);      
      $this->find_two_pair_hand($values_array, $_userDeck_array, $_dealerDeck_array);
      
      //Straigh Flush
      if( $this->_isFlush && $this->check_flush_hand_for_straight($this->_flushKeys_array ))
      {
         $this->_bestHand = self::STRAIGHT_FLUSH;
         $this->_cardKeysToHold_array = $this->_flushKeys_array;
      }
      
      //Four of Kind
      else if($this->_isFourKinds)
      {
         $this->_bestHand = self::FOUR_OF_A_KIND;
         $this->_cardKeysToHold_array = $this->_fourKinds_array;
      }
      
      //Full House
      
      //Flush
      else if($this->_isFlush)
      {
         $this->_bestHand = self::FLUSH;
         $this->_cardKeysToHold_array = $this->_flushKeys_array;
      }
      
      //Straight
      
      //Three of Kind
      else if($this->_isThreeKinds)
      {
         $this->_bestHand = self::THREE_OF_A_KIND;
         $this->_cardKeysToHold_array = $this->_threeKinds_array;
      }
      
      //Two Pair
      else if ( $this->_isTwoPairs )
      {
         $this->_bestHand = self::TWO_PAIRS;
         $this->_cardKeysToHold_array = $this->_twoPairsKeys_array;
      }
        
      //One Pair
      else if($this->_isOnePair)
      {
         $this->_bestHand = self::ONE_PAIR;
         $this->_cardKeysToHold_array = $this->_onePair_array;
      }
      
      //High Card
      else
      {
        $this->set_hand_as_high_card($_userDeck_array);
        $this->_bestHand = self::HIGHEST_CARD;
      }
      
   } //analyze_decks()
   
   
   /**
    * Checks for a flush hand
    * Saves all found flushes
    * @param array $_suits_array      a list of the user's deck's card index paired by values
    * @param array $_userDeck_array    the user's deck. These are of Cards type
    * @param array $_dealerDeck_array  the dealer's deck. These are of Cards type
    */
   private function find_flush_hand(array $_suits_array, 
                                    array $_userDeck_array,
                                    array $_dealerDeck_array)
   {
      
      $flush_array = array();     
      
      $suitCount_array = $this->get_count_by_keys($_suits_array);   
      
      foreach ($suitCount_array as $suit => $total)
      { 
         if($total === 5)
         {
            $flush_array[] = $suitCount_array[$suit]; 
            break;
         }
         
         $potentialCards_array = $this->get_potential_deck($total, 
                                                           $_userDeck_array,
                                                           $_dealerDeck_array);
         foreach ($potentialCards_array as $card)
         {
            if($card instanceof Card)
            {               
               if($card->get_suit() !== $suit) break;               
               else $total++; 
            }

            if($total === 5) $flush_array[] = $_suits_array[$suit];
         }   
            
      } //foreach suit
      
      if(count($flush_array) > 0 )
      {
         $this->_isFlush = true;
         $this->_flushKeys_array = $flush_array[0];
      }
      
   } //find_flush_hand()
   
   
   /**
    * Using the already collected Flush Keys, sorts the cards by value
    * Loops through them to check for a straight
    * Returns true if straight is found
    * @param array $_flushKeys_array   array of Card type objects already marked as a "Flush"
    */
   function check_flush_hand_for_straight(array $_flushKeys_array)
   {
      $ranks_array = $this->get_property_array('get_rank',  $_flushKeys_array);
      sort($ranks_array);
      
      $prevValue = 0; 
      $straightFound = true;
      foreach ($_flushKeys_array as $value) 
      {
         if ($prevValue == 0) $prevValue = $value;
         else if ($prevValue + 1 == $value) $prevValue = $value;
         else
         {
            $straightFound = false;
            break;
         }         
      } //foreach
      
      return $straightFound;
   } //check_flush_hand_for_straight()
   
   
   /*function find_straight_hand(array $_values_array, 
                               array $_userDeck_array,
                               array $_delearDeck_array)
   {
      
      $valueCount_array = $this->get_count_by_keys($_values_array);   
      
      //Values can only be made of one key
      foreach ($valueCount_array as $value => $total) 
      {
         if($total > 1) $_values_array[$value] = array_slice($_values_array, 0, $total - 1);
      }
      
      $straight_array   = array();     
      sort($_values_array);      
      
      foreach ($valueCount_array as $value)
      {  
         $prevValue    = 0; 
         $straightFound = true;
         foreach ($_flushKeys_array as $nextValue) 
         {
            if ($prevValue == 0) 
            {
               if($value + 1 == $nextValue)  $prevValue = $nextValue;
               else break;              
            }
            else if ($prevValue + 1 == $value) $prevValue = $value;
            else
            {
               $straightFound = false;
               break;
            }         
         } //for each value
         
         if($straightFound) $straight_array[] = $_values_array[$value]; 
      } //for each card 
   } */
   
   
   /**
    * Gets all pairs in the user's hands
    * Loops through all pairs to find a three or four of a find
    * Saves all possible pairs in arrays
    * @param array $_values_array      a list of the user's deck's card index paired by values
    * @param array $_userDeck_array    the user's deck. These are of Cards type
    * @param array $_delearDeck_array  the dealer's deck. These are of Cards type
    */
   private function find_same_value_cards(array $_values_array, 
                                          array $_userDeck_array,
                                          array $_delearDeck_array)
   {
      
      $pair_array       = array();
      $threeKinds_array = array();
      $fourKinds_array  = array();
      
      $valueCount_array = $this->get_count_by_keys($_values_array);   
      
      foreach ($valueCount_array as $value => $count)
      {  
         //Move on if its already 4 of a kind
         if($count < self::FOUR_CARDS)
         {
            $potentialCards_array = $this->get_potential_deck($count, 
                                                              $_userDeck_array,
                                                              $_delearDeck_array);
            foreach ($potentialCards_array as $card)
            {
               if($card instanceof Card)
               {               
                  if($card->get_value() !== $value) continue;               
                  else $count++; 
               }
            }           
         } //$count < self::FOUR_CARDS
         
         //Update the arrays
         if($count === self::PAIR_CARDS) $pair_array[]        = $_values_array[$value];
         if($count === self::THREE_CARDS) $threeKinds_array[] = $_values_array[$value];
         if($count === self::FOUR_CARDS) $fourKinds_array[]   = $_values_array[$value];           
         
      } //foreach card values
      
      //Check for best same card hand
      if ( count($fourKinds_array) > 0) 
      {
         $this->_isFourKinds     = true;
         $this->_fourKinds_array = $fourKinds_array[0]; 
      }
      else if ( count($threeKinds_array) > 0)
      {
         $this->_isThreeKinds     = true;
         $this->_threeKinds_array = $threeKinds_array[0];
      }
      else if ( count($pair_array) > 0)
      {
         $this->_isOnePair     = true;
         $this->_onePair_array = $pair_array[0];
      }
      
   } //find_same_value_cards()
   
     
   /**
    * Trys to find two pairs 
    * Loops through all pairs to find a three or four of a find
    * Saves all possible pairs in arrays
    * @param array $_values_array      a list of the user's deck's card index paired by values
    * @param array $_userDeck_array    the user's deck. These are of Cards type
    * @param array $_delearDeck_array  the dealer's deck. These are of Cards type
    */
   private function find_two_pair_hand(array $_values_array, 
                                       array $_userDeck_array,
                                       array $_delearDeck_array)
   {
      
      $pairOne_array = array();
      $pairTwo_array = array();
      
      $valueCount_array = $this->get_count_by_keys($_values_array);   
      
      valueLoop:foreach ($valueCount_array as $value => $count)
      { 
         $potentialCards_array = $this->get_potential_deck($count, 
                                                           $_userDeck_array,
                                                           $_delearDeck_array);
         foreach ($potentialCards_array as $card)
         {
            if($count === self::PAIR_CARDS)
            {
               if(count($pairOne_array) === 0) 
               {
                  $pairOne_array = $_values_array[$value];
                  break;
               }
               else if( count($pairTwo_array) === 0) 
               {
                  $pairTwo_array = $_values_array[$value];
                  break;
               }
               else
               {
                  break 2; //out of valueLoop
               }
            }

            if($card instanceof Card)
            {               
               if($card->get_value() !== $value) continue;               
               else $count++; 
            }              
         } //foreach potential card 
         
      } //foreach value card   
            
      if(count($pairOne_array) > 0 && count($pairTwo_array) > 0)
      {
         //Prevent the possibility of 3 indexes 
         if( count($pairOne_array) > self::PAIR_CARDS ) array_splice($pairOne_array, 0, self::PAIR_CARDS);
         if( count($pairTwo_array) > self::PAIR_CARDS ) array_splice($pairTwo_array, 0, self::PAIR_CARDS);
         
         $this->_isTwoPairs = true;
         $this->_twoPairsKeys_array = array_merge($pairOne_array, $pairTwo_array);
         sort($this->_twoPairsKeys_array);
      }             
   } //find_two_pair_hand()  
   
   
   /**
    * Loops through the user's deck
    * Saves all keys for cards of that have a rank of 7 or higher 
    * Since 7 is the lowest-high card 
    * All other cards will be discarded regardless of the cards that will be 
    * collected from the delear's deck
    * @param type $_userDeck_array
    */
   private function set_hand_as_high_card($_userDeck_array)
   {
       foreach($_userDeck_array as $key => $card )
       {
           if( !($card instanceof Card) ) break;           
           if( $card->get_rank() > self::LOWEST_HIGH_CARD ) $this->_cardKeysToHold_array[] = $key; 
       }             
   } //get_high_cards()
   
   
     /**
    * Returns the new array of cards that can be collected from the delear
    * Total new cards is determined by the remainder of total kept cards and 
    * user's total card deck 
    * @param int $_cardsKept           total number of cards that will be kept
    * @param array $_userDeck_array    user's deck
    * @param array $_delearDeck_array  delear's deck
    * @return array
    */
   private function get_potential_deck($_cardsKept, array $_userDeck_array, 
                                       array $_delearDeck_array)
   {
      $remainder = $this->get_remaining_card_total($_cardsKept, $_userDeck_array);
      return $this->get_next_cards_from_dealer($remainder, $_delearDeck_array);
   } //get_potential_deck()
    

    /**
    * Sorts the provided rank_array by low - hight values
    * Runs a loop to check if that next value is a sequence of the previous
    * If all completes successfully, returns true
    * @param array $_ranks_array
    * @return boolean
    */
   private function ranks_are_a_sequence(array $_ranks_array)
   {
      asort($_ranks_array);
      
      for($i = 1; $i < count($_ranks_array); $i++)
      {
         $previous = $_ranks_array[$i - 1];
         $current  = $_ranks_array[$i];
         
         if($current - $previous != 1) return false;
      }
      
      return true;
   } //ranks_are_a_sequence()
   
   
   /**
    * Returns the difference between the count and the deck's size
    * Used to determine how many cards can be discarded 
    * @param int   $_count       total cards to "keep" or "hold"
    * @param array $_deck_array  deck to "hold" them from
    * @return int
    */
   private function get_remaining_card_total($_count, array $_deck_array)
   {
      return count($_deck_array) - $_count;
   } //get_remaining_card_total()

   
   /**
    * Returns the next available cards from the dealer's deck based on the 
    * number of cards to be discarded. The returned array are card objects
    * @param int   $_count       total cards to "keep" or "hold"
    * @param array $_deck_array  deck to "hold" them from
    * @return array
    */
   private function get_next_cards_from_dealer($_count, array $_deck_array)
   {
      return array_slice($_deck_array, 0, $_count);
   } //get_next_cards_from_dealer()

   
   /**
    * Rerturns a value count associated by keys in descending order
    * @param array $count_array associative array containing the total value count
    */
   private function get_count_by_keys(array $_array)
   {
      $count_array = array();
      
      foreach($_array as $key => $value)
      {
         $count_array[$key] = count($_array[$key]);
      }
      
      arsort($count_array);
      return $count_array;
   } //get_suit_count()
   
   
   /**
    * Creates an associative array with the keys as the specified property
    * from the card object and the value as the "index" of the original card on deck
    * @param  string   $func           name of Card's getter for desired property     
    * @param  array    $_cards_array   array containing all card objects to collect from
    * @return array
    */
   private function get_property_array($func, array $_cards_array)
   {
      $property_array = array();      
      $index = 0;
      
      foreach($_cards_array as $card)
      {
         if($card instanceof Card)
            if( method_exists( 'Card' , $func ) )
               $property_array[ $card->$func() ][] = $index++;
      }
      
      return $property_array; 
   } //get_cards_property()     
   
   
   /**
    * Returns a non-associative array, with all the ranks for the specified 
    * deck in decending order
    * @param array $_deck_array
    */
   private function get_ranks_array(array $_deck_array)
   {
       $ranks_array = array();
       
       foreach($_deck_array as $card)
       {
           if($card instanceof Card)
           $ranks_array[] = $card->get_rank();
       }
       
       return $ranks_array;
   } //get_ranks_array()
   
} //class