<?php

define('DS', DIRECTORY_SEPARATOR);
define('ROOT', dirname(__FILE__));
define('LIBRARY_PATH',    ROOT . DS . 'library'     . DS);

//Initialize Application
require_once (LIBRARY_PATH . 'bootstrap.php');